﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Research_Project
{
    class Field
    {
        private string value;
        private int number;

        public Field(string aValue, int aNumber)
        {
            value = aValue;
            number = aNumber;
        }

        public Field()
        {
            value = "";
            number = -1;
        }

        public string Value
        {
            get
            {
                return value;
            }

            set
            {
                this.value = value;
            }
        }

        public int Number
        {
            get
            {
                return number;
            }

            set
            {
                number = value;
            }
        }
    }
}
