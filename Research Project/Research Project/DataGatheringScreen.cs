﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using com.upvise.client;
using System.IO;
using CsvHelper;
using System.Dynamic;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Research_Project
{
    public partial class Data_gathering_screen : System.Windows.Forms.Form
    {
        private Query query;

        public Data_gathering_screen(Query aQuery)
        {
            InitializeComponent();

            query = aQuery;

            //Get the templates that exist in the database and save them to a list
            JSONObject[] formTemplates = query.select("Forms.templates", "counter > 20");

            string[] templateNames = new string[formTemplates.Length];

            for (int i = 0; i < formTemplates.Length; i++)
                templateNames[i] = (string)formTemplates[i].get("name");

            formTemplate_listBox.Items.AddRange(templateNames);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (string selectedItem in formTemplate_listBox.SelectedItems)
            {
                JSONObject[] formTemplate = query.select("Forms.templates", "name='" + selectedItem + "'");

                JSONObject[] forms = query.select("Forms.forms", "templateid='" + formTemplate[0].get("id") + "' AND value!='{}'");
                List<JSONObject> allForms = new List<JSONObject>(forms.Concat(query.selectArchived("Forms.forms", "templateid='" + formTemplate[0].get("id") + "' AND value!='{}'")));
                writeToCSV(selectedItem, prepareForCSV(allForms.ToArray(), "forms", (string)formTemplate[0].get("id")));

                Console.WriteLine(selectedItem.ToString() + " " + allForms.Count);
            }

            getData();
        }

        private void getData()
        {
            JSONObject[] assets = null;
            JSONObject[] sites = null;
            JSONObject[] tasks = null;
            JSONObject[] contacts = null;
            JSONObject[] companies = null;
            JSONObject[] groups = null;
            JSONObject[] regions = null;
            JSONObject[] jobs = null;
            JSONObject[] projects = null;
            JSONObject[] activities = null;
            JSONObject[] equipment = null;
            JSONObject[] timeEntries = null;
            JSONObject[] timeTemplates = null;
            JSONObject[] fields = fields = query.select("Forms.fields", "");
            JSONObject[] punchitems = punchitems = query.select("Forms.punchitems", "");

            if (assets_checkbox.Checked)
            {
                assets = query.select("Assets.assets", "");
                sites = query.select("Assets.sites", "");
                Console.WriteLine("Number of assets entries: " + assets.Length);

                writeToCSV("assets", prepareForCSV(assets, "assets"));
                writeToCSV("sites", prepareForCSV(sites, "sites"));
            }

            if (tasks_checkbox.Checked)
            {
                tasks = query.select("Tasks.tasks", "");
                Console.WriteLine("Number of tasks entries: " + tasks.Length);

                writeToCSV("tasks", prepareForCSV(tasks, "tasks"));
            }

            if (contacts_checkbox.Checked)
            {
                contacts = query.select("Contacts.contacts", "");
                companies = query.select("Contacts.companies", "");
                groups = query.select("Contacts.groups", "");
                regions = query.select("Contacts.regions", "");

                Console.WriteLine("Number of contacts entries: " + contacts.Length);
                Console.WriteLine("Number of companies entries: " + companies.Length);
                Console.WriteLine("Number of groups entries: " + groups.Length);
                Console.WriteLine("Number of regions entries: " + regions.Length);

                writeToCSV("contacts", prepareForCSV(contacts, "contacts"));
                writeToCSV("companies", prepareForCSV(companies, "companies"));
                writeToCSV("groups", prepareForCSV(groups, ""));
                writeToCSV("regions", prepareForCSV(regions, ""));
            }

            if (jobs_checkbox.Checked)
            {
                jobs = query.select("Jobs.jobs", "");
                Console.WriteLine("Number of jobs entries: " + jobs.Length);

                writeToCSV("jobs", prepareForCSV(jobs, "jobs"));
            }

            if (projects_checkbox.Checked)
            {
                projects = query.select("Projects.projects", "");
                activities = query.select("Projects.projectactivities", "");

                Console.WriteLine("Number of projects entries: " + projects.Length);
                Console.WriteLine("Number of activities entries: " + activities.Length);

                writeToCSV("projects", prepareForCSV(projects, "projects"));
                writeToCSV("activities", prepareForCSV(activities, ""));
            }

            if (equipment_checkbox.Checked)
            {
                equipment = query.select("Tools.tools", "");
                Console.WriteLine("Number of equipment entries: " + equipment.Length);

                writeToCSV("equipment", prepareForCSV(equipment, "tools"));
            }

            if (timesheets_checkbox.Checked)
            {
                timeEntries = query.select("Time.slots", "");
                timeTemplates = query.select("Time.templates", "");

                Console.WriteLine("Number of time entries: " + timeEntries.Length);
                Console.WriteLine("Number of templates entries: " + timeTemplates.Length);

                writeToCSV("time_entries", prepareForCSV(timeEntries, ""));
                writeToCSV("time_templates", prepareForCSV(timeTemplates, ""));
            }
        }

        /**
         * Converts the data that Upvise provides into a format that will be usable in a CSV writing function. Handles 
         * custom fields and value fields in Upvise Forms so that each field is appended to the end of the typical header
         * 
         * @parma data - An array of data that Upvise has returned from things like queries
         * @param table - The last portion of the table string used to find appropriate custom fields
         * @return - The data represented in a format that is more appropriate for the CSV writing function
         */
        private List<List<string>> prepareForCSV(JSONObject[] data, string table, string templateid = "")
        {
            List<List<string>> result = new List<List<string>>();
            List<int> customHeaderNumbers = new List<int>();

            //Prepare the header for the CSV
            if (data.Length > 0)
            {
                List<string> header = new List<string>();

                foreach (string key in data[0].keys())
                {
                    if (!key.Equals("custom") && !key.Equals("value"))
                        header.Add(key);
                }

                List<string> customHeader = null;

                //If we are dealing with forms, we will need to grab the value field, not the custom field
                if (table.Equals("forms"))
                {
                    customHeader = getValueHeader(templateid);
                }
                else if (!table.Equals(""))
                    customHeader = getCustomFieldHeaders(table);

                customHeaderNumbers = getOrderedNumbers(customHeader); 


                

                //Append custom field headers on to the end of the data
                foreach (int key in customHeaderNumbers)
                {
                    foreach (string name in customHeader)
                    {
                        string temp = name.Split(new string[] { " - " }, StringSplitOptions.None)[0];
                        temp = temp.Substring(1, temp.Length - 1);

                        if (key == int.Parse(temp))
                            header.Add(name);
                    }
                }

                result.Add(header);
            }

            for (int i = 0; i < data.Length; i++)
            {
                List<Field> dataToAppend = new List<Field>();
                List<string> currentRecord = new List<string>();

                foreach (string key in data[i].keys())
                {
                    if (!key.Equals("custom") && !key.Equals("value"))
                        currentRecord.Add(Convert.ToString(data[i].get(key)));
                    else if (data[i].get(key) != null && (string)data[i].get(key) != "")
                        dataToAppend = JSONFieldsToCSV((string)data[i].get(key));
                }

                //Add all of the custom fields and value fields of forms to the CSV file
                foreach (int customFieldHeaderNumber in customHeaderNumbers)
                {
                    bool found = false;

                    foreach (Field customField in dataToAppend)
                    {
                        if (customFieldHeaderNumber == customField.Number)
                        {
                            found = true;
                            currentRecord.Add(customField.Value);
                            break;
                        }
                    }

                    if (!found)
                        currentRecord.Add("");
                }

                result.Add(currentRecord);
            }

            return result;
        }

        /**
         * Receives a list of the concatenated header names  and ID's, strips them down the the number portion of the ID and orders them
         * from lowest to highest
         * 
         * @param names - The list of ID's + names to go order
         * @return - A list containing the number portion of the ID in order from lowest to highest
         */
        private List<int> getOrderedNumbers(List<string> names)
        {
            List<int> numbers = new List<int>();

            //Remove the F and the appended label text and add the remaining number to an array
            foreach (string name in names)
            {
                string temp = name.Split(new string[] { " - " }, StringSplitOptions.None)[0];
                temp = temp.Substring(1, temp.Length - 1);
                numbers.Add(int.Parse(temp));
            }

            //Sort the List
            numbers.Sort();

            return numbers;
        }

        /**
         * Writes the data received to a CSV file with the name specified.
         * 
         * @param filename - The filename of the CSV file to write to (not including file path or file extension)
         * @param data - A List representing the records to be saved to each row of the CSV, where each record contains numerous string values
         */
        private void writeToCSV(string filename, List<List<string>> data)
        {
            using (TextWriter writer = new StreamWriter(@"C:\Users\User\Desktop\" + filename + ".csv"))
            {
                CsvWriter csv = new CsvWriter(writer);

                foreach (List<string> record in data)
                {
                    foreach (string entry in record)
                    {
                        csv.WriteField(entry);
                    }

                    csv.NextRecord();
                }

                writer.Close();
            }
        }

        /**
         * Gets the fields associated with a particular form template and returns them in a list
         * 
         * @param templateid - The Upvise ID of the template to get the fields for
         * @return - A list of strings reprenting the different field id's concatenated with their label
         */
        private List<string> getValueHeader(string templateid)
        {
            List<string> result = new List<string>();

            JSONObject[] headers = query.select("Forms.fields", "formid='" + templateid + "'");

            for (int i = 0; i < headers.Length; i++)
                result.Add(headers[i].get("name") + " - " + headers[i].get("label"));

            return result;
        }

        /**
         * Gets the custom fields associated with a particular table and returns them in a list
         * 
         * @param linkedid - The table name to the the custom fields for (refer to database "formid" column to find the different 
         *                  options
         * @return - A list of strings reprenting the different field id's concatenated with their label
         */
        private List<string> getCustomFieldHeaders(string linkedid)
        {
            List<string> result = new List<string>();

            JSONObject[] headers = query.select("Notes.fields", "formid='" + linkedid + "'");

            for (int i = 0; i < headers.Length; i++)
                result.Add(headers[i].get("name") + " - " + headers[i].get("label"));

            return result;
        }

        /**
         * Receives a JSON strng and converts it into a list of Field objects ready for more accessibility of data
         * 
         * @param values - The JSON string to convert
         * @return - A list of Field objects each representing an individual portion of the JSON string
         */ 
        private List<Field> JSONFieldsToCSV(string values)
        {
            List<Field> result = new List<Field>();
            JObject json = JObject.Parse(values);

            foreach(var current in json)
            {
                Field currentField = new Field();

                string temp = current.Key.Split(new string[] { " - " }, StringSplitOptions.None)[0];
                temp = temp.Substring(1, temp.Length - 1);
                currentField.Number = int.Parse(temp);

                currentField.Value = (string)(current.Value);
                result.Add(currentField);
            }

            return result;
        }

        /**
         * This is simply code for a test button. You may replace it with whatever test is relevant at the time
         */
        private void button1_Click_1(object sender, EventArgs e)
        {
            List<string> names = new List<string>();
            names.Add("F1 - yesasf");
            names.Add("F2 - auyuhjakcs");
            names.Add("F5 - ahsjbda");
            names.Add("F11 - jasfkc");
            names.Add("F4 - ");
            List<int> numbers = getOrderedNumbers(names);

            foreach (int number in numbers)
            {
                Console.WriteLine(number);
            }
        }
    }
}
