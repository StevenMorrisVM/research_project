﻿namespace Research_Project
{
    partial class Data_gathering_screen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.formTemplate_listBox = new System.Windows.Forms.ListBox();
            this.formTemplateCSV_button = new System.Windows.Forms.Button();
            this.timesheets_checkbox = new System.Windows.Forms.CheckBox();
            this.equipment_checkbox = new System.Windows.Forms.CheckBox();
            this.projects_checkbox = new System.Windows.Forms.CheckBox();
            this.jobs_checkbox = new System.Windows.Forms.CheckBox();
            this.contacts_checkbox = new System.Windows.Forms.CheckBox();
            this.tasks_checkbox = new System.Windows.Forms.CheckBox();
            this.assets_checkbox = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Forms";
            // 
            // formTemplate_listBox
            // 
            this.formTemplate_listBox.FormattingEnabled = true;
            this.formTemplate_listBox.Location = new System.Drawing.Point(12, 29);
            this.formTemplate_listBox.Name = "formTemplate_listBox";
            this.formTemplate_listBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.formTemplate_listBox.Size = new System.Drawing.Size(235, 537);
            this.formTemplate_listBox.Sorted = true;
            this.formTemplate_listBox.TabIndex = 1;
            // 
            // formTemplateCSV_button
            // 
            this.formTemplateCSV_button.Location = new System.Drawing.Point(12, 581);
            this.formTemplateCSV_button.Name = "formTemplateCSV_button";
            this.formTemplateCSV_button.Size = new System.Drawing.Size(373, 23);
            this.formTemplateCSV_button.TabIndex = 2;
            this.formTemplateCSV_button.Text = "Save template data to CSV";
            this.formTemplateCSV_button.UseVisualStyleBackColor = true;
            this.formTemplateCSV_button.Click += new System.EventHandler(this.button1_Click);
            // 
            // timesheets_checkbox
            // 
            this.timesheets_checkbox.AutoSize = true;
            this.timesheets_checkbox.Location = new System.Drawing.Point(280, 29);
            this.timesheets_checkbox.Name = "timesheets_checkbox";
            this.timesheets_checkbox.Size = new System.Drawing.Size(80, 17);
            this.timesheets_checkbox.TabIndex = 10;
            this.timesheets_checkbox.Text = "Timesheets";
            this.timesheets_checkbox.UseVisualStyleBackColor = true;
            // 
            // equipment_checkbox
            // 
            this.equipment_checkbox.AutoSize = true;
            this.equipment_checkbox.Location = new System.Drawing.Point(280, 59);
            this.equipment_checkbox.Name = "equipment_checkbox";
            this.equipment_checkbox.Size = new System.Drawing.Size(76, 17);
            this.equipment_checkbox.TabIndex = 11;
            this.equipment_checkbox.Text = "Equipment";
            this.equipment_checkbox.UseVisualStyleBackColor = true;
            // 
            // projects_checkbox
            // 
            this.projects_checkbox.AutoSize = true;
            this.projects_checkbox.Location = new System.Drawing.Point(280, 89);
            this.projects_checkbox.Name = "projects_checkbox";
            this.projects_checkbox.Size = new System.Drawing.Size(64, 17);
            this.projects_checkbox.TabIndex = 12;
            this.projects_checkbox.Text = "Projects";
            this.projects_checkbox.UseVisualStyleBackColor = true;
            // 
            // jobs_checkbox
            // 
            this.jobs_checkbox.AutoSize = true;
            this.jobs_checkbox.Location = new System.Drawing.Point(280, 119);
            this.jobs_checkbox.Name = "jobs_checkbox";
            this.jobs_checkbox.Size = new System.Drawing.Size(48, 17);
            this.jobs_checkbox.TabIndex = 13;
            this.jobs_checkbox.Text = "Jobs";
            this.jobs_checkbox.UseVisualStyleBackColor = true;
            // 
            // contacts_checkbox
            // 
            this.contacts_checkbox.AutoSize = true;
            this.contacts_checkbox.Location = new System.Drawing.Point(280, 149);
            this.contacts_checkbox.Name = "contacts_checkbox";
            this.contacts_checkbox.Size = new System.Drawing.Size(68, 17);
            this.contacts_checkbox.TabIndex = 14;
            this.contacts_checkbox.Text = "Contacts";
            this.contacts_checkbox.UseVisualStyleBackColor = true;
            // 
            // tasks_checkbox
            // 
            this.tasks_checkbox.AutoSize = true;
            this.tasks_checkbox.Location = new System.Drawing.Point(280, 179);
            this.tasks_checkbox.Name = "tasks_checkbox";
            this.tasks_checkbox.Size = new System.Drawing.Size(55, 17);
            this.tasks_checkbox.TabIndex = 15;
            this.tasks_checkbox.Text = "Tasks";
            this.tasks_checkbox.UseVisualStyleBackColor = true;
            // 
            // assets_checkbox
            // 
            this.assets_checkbox.AutoSize = true;
            this.assets_checkbox.Location = new System.Drawing.Point(280, 209);
            this.assets_checkbox.Name = "assets_checkbox";
            this.assets_checkbox.Size = new System.Drawing.Size(57, 17);
            this.assets_checkbox.TabIndex = 16;
            this.assets_checkbox.Text = "Assets";
            this.assets_checkbox.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(273, 433);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 17;
            this.button1.Text = "Testing button";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // Data_gathering_screen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(397, 616);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.assets_checkbox);
            this.Controls.Add(this.tasks_checkbox);
            this.Controls.Add(this.contacts_checkbox);
            this.Controls.Add(this.jobs_checkbox);
            this.Controls.Add(this.projects_checkbox);
            this.Controls.Add(this.equipment_checkbox);
            this.Controls.Add(this.timesheets_checkbox);
            this.Controls.Add(this.formTemplateCSV_button);
            this.Controls.Add(this.formTemplate_listBox);
            this.Controls.Add(this.label1);
            this.Name = "Data_gathering_screen";
            this.Text = "Data for: ";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox formTemplate_listBox;
        private System.Windows.Forms.Button formTemplateCSV_button;
        private System.Windows.Forms.CheckBox timesheets_checkbox;
        private System.Windows.Forms.CheckBox equipment_checkbox;
        private System.Windows.Forms.CheckBox projects_checkbox;
        private System.Windows.Forms.CheckBox jobs_checkbox;
        private System.Windows.Forms.CheckBox contacts_checkbox;
        private System.Windows.Forms.CheckBox tasks_checkbox;
        private System.Windows.Forms.CheckBox assets_checkbox;
        private System.Windows.Forms.Button button1;
    }
}