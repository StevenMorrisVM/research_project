﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using com.upvise.client;

namespace Research_Project
{
    public partial class Form1 : System.Windows.Forms.Form
    {
        private Query clientQuery;
        private string clientToken;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string username = username_textField.Text;
            string password = password_textField.Text;

            if (username != "" && password != "")
                login(username, password);
        }

        private void login(string username, string password)
        {
            try
            {
                clientToken = Query.login(username, password);
                clientQuery = new Query(clientToken);
                Console.WriteLine("Login successful");
            }
            catch (System.Exception e)
            {
                Console.WriteLine("Could not login: Username or password incorrect");
                Console.WriteLine(e.ToString());
            }

            Data_gathering_screen mainScreen = new Data_gathering_screen(clientQuery);
            mainScreen.Show();
        }
    }
}
